/*
ID: cbertra1
LANG: JAVA
TASK: ariprog 
 */

import java.io.*;
import java.util.*;

class ariprog {

    static String TASK_NAME = "ariprog";
    
    static int N, M;
    static PrintWriter out;
    static boolean[] bisquares;
    
    public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(TASK_NAME + ".in"));
		out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
		
		N = Integer.parseInt(in.readLine());
		M = Integer.parseInt(in.readLine());
		bisquares = new boolean[2*M*M+1];
		
		for (int i = 0; i < 2*M*M+1; i++) {
			bisquares[i] = false;
		}
		
		for (int p = 0; p <= M; p++) {
			for (int q = p; q <= M; q++) {
				bisquares[p*p+q*q] = true;
			}
		}
		
		for (int p = 0; p <= M; p++) {
			for (int q = p; q <= M; q++) {
				solve(p*p+q*q, 1, 1);
			}
		}
		
		Collections.sort(solutions, new Comparator<int[]>() {
			public int compare(int[] a, int[] b) {
				for (int i = 1; i >= 0; i--) {
					if (a[i] < b[i])
						return -1;
					else if (a[i] > b[i])
						return 1;
				}
				return 0;
			}
		});
		
		int[] last = {-1, -1};
		for (int i = 0; i < solutions.size(); i++) {
			if (last[0] == solutions.get(i)[0] && last[1] == solutions.get(i)[1])
				continue;
			out.println(solutions.get(i)[0] + " " + solutions.get(i)[1]);
			last = solutions.get(i);
		}
		
		if (solutions.size() == 0)
			out.println("NONE");
		
		out.close();
		in.close();
    }
    
    static ArrayList<int[]> solutions = new ArrayList<int[]>();
    
    static void solve(int a, int b, int n) {
    	if (n == N) {
    		solutions.add(new int[]{a, b});
    		return;
    	}
    	if (n == 1 && b <= 2*M*M/(N-1)) {
    		solve(a, b+1, 1);
    	}
    	
    	if (a+n*b <= 2*M*M && bisquares[a+n*b]) {
    		solve(a, b, n+1);
    	}
    }
}
