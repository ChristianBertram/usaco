/*
ID: cbertra1
LANG: JAVA
TASK: dualpal
 */

import java.io.*;
import java.util.*;

class dualpal {

    static String TASK_NAME = "dualpal";
    
    public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(TASK_NAME + ".in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
		
		StringTokenizer st = new StringTokenizer(in.readLine());
		
		int N = Integer.parseInt(st.nextToken());
		int S = Integer.parseInt(st.nextToken());
		
		int found = 0;
		for (int ns = S+1; found < N; ns++) {
			int palCount = 0;
			for (int b = 2; b <= 10; b++) {
				if (palindrome(intToBase(ns, b)))
					palCount++;
				if (palCount >= 2) {
					out.println(ns);
					found++;
					break;
				}
			}
		}
		
		out.close();
		in.close();
    }
	
	static boolean palindrome(String val) {
		for (int i = 0; i < val.length()/2; i++) {
			if (val.charAt(i) != val.charAt(val.length()-1-i))
				return false;
		}
		return true;
	}
	
	static String intToBase(int val, int base) {
		String valStr = "";
		valStr += val%base;
		val /= base;
		while (val != 0) {
			valStr += val%base;
			val /= base;
		}
		return valStr;
	}
}
