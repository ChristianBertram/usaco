/*
ID: cbertra1
LANG: JAVA
TASK: ride
 */

import java.io.*;
import java.util.*;

class ride {

    static String TASK_NAME = "ride";
    
    public static void main(String[] args) throws IOException {
	BufferedReader br = new BufferedReader(new FileReader(TASK_NAME + ".in"));
	PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
	
	String cometStr = br.readLine();
	String groupStr = br.readLine();

	int comet = 1;
	int group = 1;
	
	for (int i = 0; i < cometStr.length(); i++) {
	    comet = comet * ((int)cometStr.charAt(i) - (int)'A' + 1) % 47;
	}

	for (int i = 0; i < groupStr.length(); i++) {
	    group = group * ((int)groupStr.charAt(i) - (int)'A' + 1) % 47;
	}

	if (comet == group)
	    out.println("GO");
	else
	    out.println("STAY");
	
	out.close();
	br.close();
    }
}
