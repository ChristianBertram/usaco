/*
ID: cbertra1
LANG: JAVA
TASK: crypt1
 */

import java.io.*;
import java.util.*;

class crypt1 {

    static String TASK_NAME = "crypt1";
    
    public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(TASK_NAME + ".in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
		
		int N = Integer.parseInt(in.readLine());
		
		StringTokenizer st = new StringTokenizer(in.readLine());
		SortedSet<Integer> digitsSet = new TreeSet<Integer>();
		for (int i = 0; i < N; i++) {
			int digit = Integer.parseInt(st.nextToken());
			if (digit > 0 && digit < 10)
				digitsSet.add(digit);
		}
		int[] digits = new int[digitsSet.size()];
		int index = 0;
		for (Integer i : digitsSet) {
			digits[index++] = i;
		}
		
		int[] d = digits;
		
		int count = 0;
		
		for (int i1 = 0; i1 < digits.length; i1++) {
			for (int i2 = 0; i2 < digits.length; i2++) {
				for (int i3 = 0; i3 < digits.length; i3++) {
					for (int j1 = 0; j1 < digits.length; j1++) {
						for (int j2 = 0; j2 < digits.length; j2++) {
							int n1 = d[i3]*100+d[i2]*10+d[i1];
							int n2 = d[j2]*10+d[j1];
							
							int p1 = n1*d[j1];
							int p2 = n1*d[j2];
							
							int r = n1*n2;
							
							if (
									contains(digitsSet, n1%10)
									&& contains(digitsSet, n1/10%10)
									&& contains(digitsSet, n1/100)
									
									&& contains(digitsSet, n2%10)
									&& contains(digitsSet, n2/10)
									
									&& contains(digitsSet, p1%10)
									&& contains(digitsSet, p1/10%10)
									&& contains(digitsSet, p1/100)
									
									&& contains(digitsSet, p2%10)
									&& contains(digitsSet, p2/10%10)
									&& contains(digitsSet, p2/100)
									
									&& contains(digitsSet, r%10)
									&& contains(digitsSet, r/10%10)
									&& contains(digitsSet, r/100%10)
									&& contains(digitsSet, r/1000)
								)
								count++;
						}
					}
				}
			}
		}
		
		out.println(count);
		
		out.close();
		in.close();
    }
    
    static boolean contains(Set s, int k) {
    	return s.contains(Integer.valueOf(k));
    }
}
