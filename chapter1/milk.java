/*
ID: cbertra1
LANG: JAVA
TASK: milk
 */

import java.io.*;
import java.util.*;

class milk {

    static String TASK_NAME = "milk";
    
    public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(TASK_NAME + ".in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
		
		StringTokenizer st = new StringTokenizer(in.readLine());
		int N = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		
		int[][] farmers = new int[M][2];
		
		for (int i = 0; i < M; i++) {
			st = new StringTokenizer(in.readLine());
			farmers[i][0] = Integer.parseInt(st.nextToken());
			farmers[i][1] = Integer.parseInt(st.nextToken());
		}
		
		Arrays.sort(farmers, new Comparator<int[]>() {
			@Override
			public int compare(int[] o1, int[] o2) {
				return (new Integer(o1[0])).compareTo(new Integer(o2[0]));
			}
		});
		
		int currPrice = 0;
		int currUnits = 0;
		for (int i = 0; i < M && N-currUnits > 0; i++) {
			if (N-currUnits >= farmers[i][1]) {
				currPrice += farmers[i][0]*farmers[i][1];
				currUnits += farmers[i][1];
			}
			else {
				int unitsNeeded = N-currUnits;
				currPrice += farmers[i][0]*unitsNeeded;
				currUnits += unitsNeeded;
			}
		}
		
		out.println(currPrice);
		
		out.close();
		in.close();
    }
}
