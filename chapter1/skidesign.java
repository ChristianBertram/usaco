/*
ID: cbertra1
LANG: JAVA
TASK: skidesign
 */

import java.io.*;
import java.util.*;

class skidesign {

    static String TASK_NAME = "skidesign";
    
    public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(TASK_NAME + ".in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
		
		int N = Integer.parseInt(in.readLine());
		
		int[] hills = new int[N];
		for (int i = 0; i < N; i++) {
			hills[i] = Integer.parseInt(in.readLine());
		}
		
		int bestCost = 1000000000;
		for (int i = 0; i <= 100; i++) {
			int cost = cost(i, hills);
			
			if (cost < bestCost)
				bestCost = cost;
		}
		
		out.println(bestCost);
		
		out.close();
		in.close();
    }
    
    static int cost(int start, int[] hills) {
    	int cost = 0;
    	for (int h : hills) {
    		if (h < start)
    			cost += Math.pow(start-h, 2);
    		else if (h-start > 17)
    			cost += Math.pow(h-start-17, 2);
    	}
    	return cost;
    }
}
