/*
ID: cbertra1
LANG: JAVA
TASK: friday
 */

import java.io.*;
import java.util.*;

class friday {

    static String TASK_NAME = "friday";
    
    public static void main(String[] args) throws IOException {
	BufferedReader br = new BufferedReader(new FileReader(TASK_NAME + ".in"));
	PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
	
	int n = Integer.parseInt(br.readLine());
	
	int totalDays = 0;
	int[] daysCount = new int[7];
	for (int y = 1900; y < 1900+n; y++) {
		for (int m = 1; m <= 12; m++) {
			daysCount[(totalDays+13)%7]++;
			totalDays += daysInMonth(m, y);
		}
	}
	
	out.println(daysCount[6] + " " + daysCount[0] + " " + daysCount[1] + " " + daysCount[2] + " " + daysCount[3] + " " + daysCount[4] + " " + daysCount[5]);
	
	out.close();
	br.close();
    }
    
    static int[] daysInMonth = {31,28,31,30,31,30,31,31,30,31,30,31};
    static int daysInMonth(int m, int y) {
    	if (m != 2) {
    		return daysInMonth[m-1];
    	}
    	else if (y % 4 == 0 && (y % 400 == 0 || y % 100 != 0)) {
    		return 29;
    	}
    	else {
    		return 28;
    	}
    }
}
