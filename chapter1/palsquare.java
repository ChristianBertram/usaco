/*
ID: cbertra1
LANG: JAVA
TASK: palsquare
 */

import java.io.*;
import java.util.*;

class palsquare {

    static String TASK_NAME = "palsquare";
    
    public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(TASK_NAME + ".in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
		
		int B = Integer.parseInt(in.readLine());

		for (int n = 1; n <= 300; n++) {
			int[] nb = intToBase(n, B);
			int[] nnb = intToBase(n*n, B);
			if (palindrome(nnb)) {
				out.println(baseToString(nb) + " " + baseToString(nnb));
			}
		}
		
		out.close();
		in.close();
    }
	
	static boolean palindrome(int[] val) {
		for (int i = 0; i < val.length/2; i++) {
			if (val[i] != val[val.length-1-i])
				return false;
		}
		return true;
	}
	
	static int[] intToBase(int val, int base) {
		ArrayList<Integer> newVal = new ArrayList<Integer>();
		newVal.add(val%base);
		val /= base;
		while (val != 0) {
			newVal.add(0, val%base);
			val /= base;
		}
		
		int[] newArr = new int[newVal.size()];
		for (int i = 0; i < newArr.length; i++) {
			newArr[i] = newVal.get(i);
		}
		return newArr;
	}
	
	static String baseToString(int[] val) {
		String valStr = "";
		for (int i : val) {
			if (i < 10)
				valStr += i;
			else 
				switch (i) {
					case 10: valStr += "A"; break;
					case 11: valStr += "B"; break;
					case 12: valStr += "C"; break;
					case 13: valStr += "D"; break;
					case 14: valStr += "E"; break;
					case 15: valStr += "F"; break;
					case 16: valStr += "G"; break;
					case 17: valStr += "H"; break;
					case 18: valStr += "I"; break;
					case 19: valStr += "J"; break;
					case 20: valStr += "K"; break;
				}
		}
		return valStr;
	}
}
