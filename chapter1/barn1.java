/*
ID: cbertra1
LANG: JAVA
TASK: barn1
 */

import java.io.*;
import java.util.*;

class barn1 {

    static String TASK_NAME = "barn1";
    
    public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(TASK_NAME + ".in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
		
		StringTokenizer st = new StringTokenizer(in.readLine());
		int M = Integer.parseInt(st.nextToken());
		int S = Integer.parseInt(st.nextToken());
		int C = Integer.parseInt(st.nextToken());
		
		Stall[] stalls = new Stall[S];
		for (int i = 0; i < S; i++) {
			stalls[i] = new Stall();
		}
		for (int i = 0; i < C; i++) {
			stalls[Integer.parseInt(in.readLine())-1].occ = true;
		}
		
		for (int i = 0; i < S && !stalls[i].occ; i++) {
			stalls[i].cov = false;
		}
		for (int i = S-1; i >= 0 && !stalls[i].occ; i--) {
			stalls[i].cov = false;
		}		
		for (int m = 2; m <= M; m++) {
			int bestStart = 0;
			int bestEnd = 0;
			int currStart = 0;
			for (int i = 0; i < S; i++) {
				if (stalls[i].cov) {
					if (!stalls[i].occ && currStart == 0)
						currStart = i;
					else if (stalls[i].occ && currStart != 0) {
						if (i-currStart > bestEnd-bestStart) {
							bestStart = currStart;
							bestEnd = i-1;							
						}
						currStart = 0;
					}
				} 
			}
			for (int i = bestStart; i <= bestEnd; i++) {
				stalls[i].cov = false;
			}
		}
		
		int covCount = 0;
		for (int i = 0; i < S; i++) {
			if (stalls[i].cov)
				covCount++;
		}
		
		out.println(covCount);
		
		out.close();
		in.close();
    }
    
    static class Stall {
    	boolean occ, cov;
    	Stall() {
    		this.occ = false;
    		this.cov = true;
    	}
    }
}
