/*
ID: cbertra1
LANG: JAVA
TASK: combo
 */

import java.io.*;
import java.util.*;

class combo {

    static String TASK_NAME = "combo";
    
    public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(TASK_NAME + ".in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
		
		int N = Integer.parseInt(in.readLine());
		StringTokenizer st = new StringTokenizer(in.readLine());
		int[] john = new int[3];
		for (int i = 0; i < 3; i++) {
			john[i] = Integer.parseInt(st.nextToken());
		}
		st = new StringTokenizer(in.readLine());
		int[] master = new int[3];
		for (int i = 0; i < 3; i++) {
			master[i] = Integer.parseInt(st.nextToken());
		}
		
		ArrayList<Integer[]> combs = new ArrayList<Integer[]>();
		for (int a = -2; a <=2; a++) {
			for (int b = -2; b <= 2; b++) {
				for (int c = -2; c <= 2; c++) {
					Integer[] newComb = new Integer[]{myMod(john[0]+a, N), myMod(john[1]+b, N), myMod(john[2]+c, N)};
					if (!(combsContains(newComb, combs)))
						//System.out.println(newComb[0] + " " + newComb[1] + " " + newComb[2]);
						combs.add(newComb);
					
					newComb = new Integer[]{myMod(master[0]+a, N), myMod(master[1]+b, N), myMod(master[2]+c, N)};
					if (!(combsContains(newComb, combs)))
						//System.out.println(newComb[0] + " " + newComb[1] + " " + newComb[2]);
						combs.add(newComb);
				}
			}
		}
		
		out.println(combs.size());
		
		in.close();
		out.close();
    }
    
    static int myMod(int a, int n) {
    	int r = a%n;
    	if (r < 0)
    		return n+r;
    	else
    		return r;
    }
    
    static boolean combsContains(Integer[] key, ArrayList<Integer[]> al) {
    	for (Integer[] ael : al) {
    		if (key[0] == ael[0] && key[1] == ael[1] && key[2] == ael[2])
    			return true;
    	}
    	return false;
    }
}
