/*
ID: cbertra1
LANG: JAVA
TASK: gift1
 */

import java.io.*;
import java.util.*;

class gift1 {
	
    static String TASK_NAME = "gift1";
    
    public static void main(String[] args) throws IOException {
	BufferedReader br = new BufferedReader(new FileReader(TASK_NAME + ".in"));
	PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
	
	Map<String, Person> map = new HashMap<String, Person>();
	
	int NP = Integer.parseInt(br.readLine());
	String[] names = new String[NP];
	for (int i = 0; i < NP; i++) {
		String name = br.readLine();
		map.put(name, new Person());
		names[i] = name;
	}
	
	for (int i = 0; i < NP; i++) {
		String currPerson = br.readLine();
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		map.get(currPerson).setStart(Integer.parseInt(st.nextToken()));
		
		int friendCount = Integer.parseInt(st.nextToken());
		for (int j = 0; j < friendCount; j++) {
			map.get(currPerson).addFriend(br.readLine());
		}
	}
	
	for (int i = 0; i < NP; i++) {
		map.get(names[i]).giveGifts(map);
	}
	
	for (int i = 0; i < NP; i++) {
		out.println(names[i] + " " + map.get(names[i]).getBalance());
	}
	
	out.close();
	br.close();
    }
    
    static class Person {
    	private int start;
    	private int curr;
    	private int gift;
    	private ArrayList<String> friends = new ArrayList<String>();
    	
    	void setStart(int s) {
    		start = s;
    	}
    	
    	void addFriend(String f) {
    		friends.add(f);
    		gift = start/friends.size();
    	}
    	
    	void giveGifts(Map<String, Person> map) {
    		for (int i = 0; i < friends.size(); i++) {
    			String friend = friends.get(i);
    			map.get(friend).recieveGift(gift);
    			curr -= gift;
    		}
    	}
    	
    	void recieveGift(int gift) {
    		curr += gift;
    	}
    	
    	int getBalance() {
    		return curr;
    	}
    }
}