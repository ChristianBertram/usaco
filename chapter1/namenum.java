/*
ID: cbertra1
LANG: JAVA
TASK: namenum
 */

import java.io.*;
import java.util.*;

class namenum {

    static String TASK_NAME = "namenum";
    
    public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(TASK_NAME + ".in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
		
		BufferedReader dict = new BufferedReader(new FileReader("dict.txt"));
		
		String[] inputStr = in.readLine().split("");
		
		boolean noName = true;
		
		for (String dictName = dict.readLine(); dictName != null; dictName = dict.readLine()) {
			boolean same = true;
			if (dictName.length() == inputStr.length) {
				for (int i = 0; i < inputStr.length; i++) {
					if (!rightChar(dictName.charAt(i), Integer.parseInt(inputStr[i]))) {
						same = false;
						break;
					}
				}
			}
			else
				same = false;
			
			if (same) {
				out.println(dictName);
				noName = false;
			}
		}
		
		if (noName)
			out.println("NONE");
		
		dict.close();
		out.close();
		in.close();
    }
    
    static boolean rightChar(char c, int i) {
    	if (Character.getNumericValue(c) < Character.getNumericValue('Q'))
    		return (Character.getNumericValue(c) - Character.getNumericValue('A'))/3+2 == i;
    	else
    		return (Character.getNumericValue(c) - Character.getNumericValue('A')-1)/3+2 == i;
    }
}
