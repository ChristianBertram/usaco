/*
ID: cbertra1
LANG: JAVA
TASK: transform
 */

import java.io.*;
import java.util.*;

class transform {

    static String TASK_NAME = "transform";
    
    public static void main(String[] args) throws IOException {
	BufferedReader br = new BufferedReader(new FileReader(TASK_NAME + ".in"));
	PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
	
	int N = Integer.parseInt(br.readLine());
	
	char[][] pattern = new char[N][N];
	
	for (int y = 0; y < N; y++) {
		String line = br.readLine();
		for (int x = 0; x < N; x++) {
			pattern[x][y] = line.charAt(x);
		}
	}
	
	char[][] endPattern = new char[N][N];
	
	for (int y = 0; y < N; y++) {
		String line = br.readLine();
		for (int x = 0; x < N; x++) {
			endPattern[x][y] = line.charAt(x);
		}
	}
	
	out.println(findMove(pattern, endPattern));
	
	out.close();
	br.close();
    }
    
    static int findMove(char[][] pattern, char[][] endPattern) {
    	for (int i = 1; i < 4; i++) {
    		if (same(rotate(pattern, i), endPattern))
    			return i;
    	}
    	
    	if (same(reflect(pattern), endPattern))
    		return 4;
    	
    	for (int i = 1; i < 4; i++) {
    		if (same(rotate(reflect(pattern), i), endPattern))
    			return 5;
    	}
    	
    	if (same(pattern, endPattern))
    		return 6;
    	
    	return 7;
    }
    
    static boolean same(char[][] arr1, char[][] arr2) {
    	for (int x = 0; x < arr1.length; x++) {
    		for (int y = 0; y < arr1.length; y++) {
    			if (arr1[x][y] != arr2[x][y]) {
    				return false;
    			}
    		}
    	}
    	return true;
    }
    
    static char[][] rotate(char[][] arr, int rotation) {
    	switch (rotation) {
    	case 1: return rotate90(arr);
    	case 2: return rotate180(arr);
    	case 3: return rotate270(arr);
    	default: return arr;
    	}
    }
    
    static char[][] rotate90(char[][] arr) {
    	char[][] newArr = new char[arr.length][arr.length];
    	for (int x = 0; x < arr.length; x++){
    		for (int y = 0; y < arr.length; y++) {
    			newArr[arr.length-1-y][x] = arr[x][y];
    		}
    	}
    	return newArr;
    }
    
    static char[][] rotate180(char[][] arr) {
    	char[][] newArr = new char[arr.length][arr.length];
    	for (int x = 0; x < arr.length; x++){
    		for (int y = 0; y < arr.length; y++) {
    			newArr[arr.length-1-x][arr.length-1-y] = arr[x][y];
    		}
    	}
    	return newArr;
    }
    
    static char[][] rotate270(char[][] arr) {
    	char[][] newArr = new char[arr.length][arr.length];
    	for (int x = 0; x < arr.length; x++){
    		for (int y = 0; y < arr.length; y++) {
    			newArr[y][arr.length-1-x] = arr[x][y];
    		}
    	}
    	return newArr;
    }
    
    static char[][] reflect(char[][] arr) {
    	char[][] newArr = new char[arr.length][arr.length];
    	for (int x = 0; x < arr.length; x++){
    		for (int y = 0; y < arr.length; y++) {
    			newArr[arr.length-1-x][y] = arr[x][y];
    		}
    	}
    	return newArr;
    }
}
