/*
.ID: cbertra1
LANG: JAVA
TASK: wormhole
 */

import java.io.*;
import java.util.*;

class wormhole {

    static String TASK_NAME = "wormhole";
    
    static int N;
    static int[] X, Y, nextOnRight, partner;
    
    public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(TASK_NAME + ".in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
		
		N = Integer.parseInt(in.readLine());
		
		X = new int[N];
		Y = new int[N];
		nextOnRight = new int[N];
		partner = new int[N];
		
		for (int i = 0; i < N; i++) {
			partner[i] = -1;
			nextOnRight[i] = -1;
		}
		
		for (int i = 0; i < N; i++) {
			StringTokenizer st = new StringTokenizer(in.readLine());
			X[i] = Integer.parseInt(st.nextToken());
			Y[i] = Integer.parseInt(st.nextToken());
		}
		
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (Y[i] == Y[j] && X[i] < X[j]) {
					if (nextOnRight[i] == -1 || X[j]-X[i] < X[nextOnRight[i]]-X[i])
						nextOnRight[i] = j;
				}
			}
		}
		System.out.println(Arrays.toString(nextOnRight));
		
		out.println(solve());
		
		out.close();
		in.close();
    }
    
    static int solve() {
    	int total = 0;
    	int i;
    	for (i = 0; i < N; i++) {
    		if (partner[i] == -1)
    			break;
    	}
    	if (i >= N) {
    		if (cycles())
    			return 1;
    		else
    			return 0;
    	}
	    	
    	for (int j = i+1; j < N; j++) {
	    	if (partner[j] == -1) {
				partner[j] = i;
				partner[i] = j;
				total += solve();
				partner[j] = -1;
				partner[i] = -1;
	    	}
	    }
    	return total;
    }
    
    static boolean cycles() {
    	int n;
    	for (int i = 0; i < N; i++) {
    		int pos = i;
    		for (n = 0; n < N; n++) {
    			if (nextOnRight[pos] == -1)
    				break;
    			else
    				pos = partner[nextOnRight[pos]];
    		}
    		if (n >= N)
    			return true;
    	}
    	return false;
    }
}