/*
ID: cbertra1
LANG: JAVA
TASK: beads
 */

import java.io.*;
import java.util.*;

class beads {

    static String TASK_NAME = "beads";
    
    public static void main(String[] args) throws IOException {
	BufferedReader br = new BufferedReader(new FileReader(TASK_NAME + ".in"));
	PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
	
	int n = Integer.parseInt(br.readLine());
	char[] necklace = br.readLine().toCharArray();
	
	int maxBeads = countBeads(necklace);
	for (int i = 0; i < necklace.length-1; i++) {
		if (necklace[i] != necklace [i+1]) {
			int currBeads = countBeads(cycleShift(necklace, i+1));
			if (currBeads > maxBeads)
				maxBeads = currBeads;
		}
	}
	
	out.println(maxBeads);
	
	out.close();
	br.close();
    }
    
    static char[] cycleShift(char[] arr, int val) {
    	char[] newArr = Arrays.copyOf(arr, arr.length);
    	for (int i = 0; i < arr.length; i++) {
    		newArr[((i-val)%arr.length+arr.length)%arr.length] = arr[i];
       	}
    	return newArr;
    }
    
    static int countBeads(char[] arr) {
    	char startColor = arr[0];
    	int count1 = 1;
    	for (int i = 1; i < arr.length; i++) {
    		if (arr[i] != startColor && startColor == 'w')
    			startColor = arr[i];
    		else if (arr[i] != startColor && arr[i] != 'w') {
    			break;
    		}
    		count1++;
    	}
    	startColor = arr[arr.length-1];
    	int count2 = 0;
    	for (int i = arr.length-1; i > count1-1; i--) {
    		if (arr[i] != startColor && startColor == 'w')
    			startColor = arr[i];
    		else if (arr[i] != startColor && arr[i] != 'w') {
    			break;
    		}
    		count2++;
    	}
    	return count1+count2;
    }
}