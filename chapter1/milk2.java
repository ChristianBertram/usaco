/*
ID: cbertra1
LANG: JAVA
TASK: milk2
 */

import java.io.*;
import java.util.*;

class milk2 {

    static String TASK_NAME = "milk2";
    
    public static void main(String[] args) throws IOException {
	BufferedReader br = new BufferedReader(new FileReader(TASK_NAME + ".in"));
	PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
	
	int N = Integer.parseInt(br.readLine());
	
	int[][] times = new int[N][2];
	
	for (int i = 0; i < N; i++) {
		int[] time = getTime(br);
		times[i][0] = time[0];
		times[i][1] = time[1];
	}
	
	Arrays.sort(times, new Comparator<int[]>() {
	    @Override
	    public int compare(int[] o1, int[] o2) {
	        //return Integer.compare(o1[0], o2[0]);
	    	return (o1[0] < o2[0]) ? -1 : ((o1[0] == o2[0]) ? 0 : 1); // USACO uses java 6 ?!
	    }
	});
	
	int[] currTime = times[0];
	int milkStart = currTime[0];
	int milkEnd = currTime[1];
	int milkMax = milkEnd-milkStart;
	int idleMax = 0;
	for (int i = 1; i < N; i++) {
		currTime = times[i];
		if (currTime[0] > milkEnd) {
			int currIdle = currTime[0]-milkEnd;
			if (currIdle > idleMax)
				idleMax = currIdle;
			milkStart = currTime[0];
			milkEnd = currTime[1];
		}
		else {
			if (currTime[1] > milkEnd)
				milkEnd = currTime[1];
			int currMilk = milkEnd-milkStart;
			if (currMilk > milkMax)
				milkMax = currMilk;
		}
	}
	
	out.println(milkMax + " " + idleMax);
	
	out.close();
	br.close();
    }
    
    static int[] getTime(BufferedReader br) throws IOException {
    	String[] sTimes = br.readLine().split(" ");
    	int[] iTimes = {Integer.parseInt(sTimes[0]), Integer.parseInt(sTimes[1])};
    	return iTimes;
    }
}