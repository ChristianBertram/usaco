/*
ID: cbertra1
LANG: JAVA
TASK: milk3
 */

import java.io.*;
import java.util.*;

class milk3 {

    static String TASK_NAME = "milk3";
    
    static int[] ABC = new int[3];
    static boolean[][] buckets; //States the buckets have already been in. Only need two dimensions, since the third is just the size of bucket C minus the contents of A and B.
    
    public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(TASK_NAME + ".in"));
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(TASK_NAME + ".out")));
		
		StringTokenizer st = new StringTokenizer(in.readLine());
		ABC[0] = Integer.parseInt(st.nextToken());
		ABC[1] = Integer.parseInt(st.nextToken());
		ABC[2] = Integer.parseInt(st.nextToken());
		
		buckets = new boolean[ABC[0]+1][ABC[1]+1];
		
		solve(new int[]{0, 0, ABC[2]});
		
		Collections.sort(sol);
		
		for (int i = 0; i < sol.size()-1; i++) {
			out.print(sol.get(i) + " ");
		}
		out.println(sol.get(sol.size()-1));
		
		out.close();
		in.close();
    }
    
    static ArrayList<Integer> sol = new ArrayList<Integer>();
    
    static void solve(int[] abc) {
    	if (buckets[abc[0]][abc[1]])
    		return;
    	if (abc[0] == 0) {
    		sol.add(abc[2]);
    	}
    	buckets[abc[0]][abc[1]] = true;
    	
    	solve(pour(abc, 0, 1));
    	solve(pour(abc, 0, 2));
    	solve(pour(abc, 1, 0));
    	solve(pour(abc, 1, 2));
    	solve(pour(abc, 2, 0));
    	solve(pour(abc, 2, 1));
    }
    
    static int[] pour(int[] abc, int from, int to) {
    	if (abc[from] == 0 || abc[to] == ABC[to])
    		return Arrays.copyOf(abc, abc.length);
    	if (abc[from]+abc[to] <= ABC[to]) {
    		int[] nabc = Arrays.copyOf(abc, abc.length);
    		nabc[from] = 0;
    		nabc[to] = abc[to]+abc[from];
    		return nabc;
    	}
    	else {
    		int[] nabc = Arrays.copyOf(abc, abc.length);
    		nabc[to] = ABC[to];
    		nabc[from] = abc[from]-(ABC[to]-abc[to]);
    		return nabc;
    	}
    }
}